package actions

import (
	"net/http"

	"github.com/gobuffalo/buffalo"
)

// HomeHandler is a default handler to serve up
// a home page.
func HomeHandler(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("home/index.plush.html"))
}

func AboutHandler(c buffalo.Context) error {
	c.Set("title", "About page")
	c.Set("content", "Content About this page")
	return c.Render(http.StatusOK, r.HTML("about.plush.html"))
}
