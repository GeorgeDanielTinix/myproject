package models

func (ms *ModelSuite) Test_User() {
	u := &User{
		FirstName: "Daniel",
		LastName:  "Tinivella",
	}

	ms.Equal("Daniel Tinivella", u.FullName(), "FullName returns user name.")
}
